<?php

require_once __DIR__ . '/base/PDF.php';
require_once __DIR__ . '/base/PDFStream.php';

/**
* A pHPDF graphics canvas.
*
* Represents a canvas.
*
* @author chervbyn
* @package pHPDF
*/
class pHPDFCanvas extends PDFCanvas {

}
