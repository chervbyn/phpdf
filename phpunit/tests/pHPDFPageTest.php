<?php
require_once __DIR__ . '/../../base/PDF.php';
require_once __DIR__ . '/../../pHPDFPage.php';

/**
 *
 */
class pHPDFPageTest extends PHPUnit_Framework_TestCase {
	/**
	 * @var pHPDFPage
	 */
	protected $object;
	protected $pdf;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->pdf = new PDF();
		$this->object = new pHPDFPage($this->pdf);
	}

	/**
	 * @covers pHPDFPage::addText
	 */
	public function testAddText() {
		$page = new pHPDFPage(new PDF());
		$text = $page->addText('Hello World!', 100, 100, 300, 300);
		$text->setFont('Arial');
		$text->setFontSize(20);
		$this->assertInstanceOf('pHPDFText', $text);
		$expected = ' BT /Arial 20 Tf 100 100 Td (Hello World!) Tj ET';
		$this->assertEquals($expected, $text->getStream());

	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testSetFont().
	 */
	public function testSetFont()
	{
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
          'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testSetFontSize().
	 */
	public function testSetFontSize()
	{
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
          'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testAddCanvas().
	 */
	public function testAddCanvas()
	{
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
          'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testAddImage().
	 */
	public function testAddImage()
	{
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
          'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers {className}::{origMethodName}
	 * @todo Implement testGetWidth().
	 */
	public function testGetWidth()
	{
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
          'This test has not been implemented yet.'
		);
	}
}
?>
