<?php

require_once 'PHPUnit/Autoload.php';
require_once __DIR__ . '/../base/PDFDictionaryTest.php';
require_once __DIR__ . '/../../../dictionaries/PDFPageTreeNode.php';
require_once __DIR__ . '/../../../dictionaries/PDFPages.php';
require_once __DIR__ . '/../../../util/PDFDictionaryParser.php';

class PDFPageTreeNodeTest extends PDFDictionaryTest {

	protected $object;
	protected $pdf;

	protected function setUp() {
		$this->pdf = new PDF();
		$this->object = new PDFPageTreeNode($this->pdf);
	}

	function test_box_setters() {
		$keys = array('MediaBox', 'ArtBox', 'CropBox', 'BleedBox', 'TrimBox');
		$entries = array();
		foreach ($keys as $key) {
			$x1 = rand(0, 500); $y1 = rand(0, 500); $x2 = rand(0, 500); $y2 = rand(0, 500);
			$fctName = "set$key";
			$this->object->$fctName($x1, $y1, $x2, $y2);
			$entries[$key] = "[ $x1 $y1 $x2 $y2 ]";
		}

		$parsed = PDFDictionaryParser::parse($this->object->__toString());
// 		$this->assertEquals(count($keys), $parsed->size());
		foreach ($keys as $key) {
			$this->assertEquals($entries[$key], $parsed->get($key));
		}
	}

	function testSetParent() {
		$pages = new PDFPages($this->pdf);
		$pagesLink = $pages->getLink();

		$node = $this->object;
		// assure it's not set
		$node->remove('Parent');
		$this->assertNull($node->get('Parent'));

		$size = $node->size();
		$this->object->setParent($pages);
		$this->assertEquals($size + 1, $node->size());
		$this->assertEquals($pagesLink, $node->get('Parent'));
	}
}