<?php

require_once __DIR__ . '/../base/PDFDictionaryTest.php';
require_once __DIR__ . '/../../../dictionaries/PDFFunction.php';

/**
 * Test class for PDFFunction.
 * Generated by PHPUnit on 2011-12-31 at 01:05:49.
 */
class PDFFunctionTest extends PDFDictionaryTest {
	/**
	 * @var PDFFunction
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = new PDFFunction(2);
	}

	function test_defaults() {
		$functionType = PDFFunction::EXPONENTIAL_INTERPOLATION;
		$pdfFunction = new PDFFunction($functionType);
			
		// Type should be Pattern
		$pattern = "/^  \/FunctionType $functionType$/m";
		$this->assertRegExp($pattern, $this->object->__toString());
	}
}
?>
