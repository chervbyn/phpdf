<?php

require_once 'PHPUnit/Autoload.php';
require_once __DIR__ . '/../../../base/PDFDictionary.php';

/**
 * A dictionary object is an associative table containing pairs of objects, known as the
 * dictionary’s entries. The first element of each entry is the key and the second element is the
 * value. The key shall be a name (unlike dictionary keys in PostScript, which may be objects of
 * any type). The value may be any kind of object, including another dictionary. A dictionary
 * entry whose value is null (see 7.3.9, "Null Object") shall be treated the same as if the entry
 * does not exist. (This differs from PostScript, where null behaves like any other object as the
 * value of a dictionary entry.) The number of entries in a dictionary shall be subject to an
 * implementation limit; see Annex C. A dictionary may have zero entries. The entries in a
 * dictionary represent an associative table and as such shall be unordered even though an
 * arbitrary order may be imposed upon them when written in a file. That ordering shall be ignored.
 * Multiple entries in the same dictionary shall not have the same key. A dictionary shall be
 * written as a sequence of key-value pairs enclosed in double angle brackets (<< ... >>) (using
 * LESS-THAN SIGNs (3Ch) and GREATER-THAN SIGNs (3Eh)). (Adobe)
 *
 * @author chervbyn
 *
 */
class PDFDictionaryTest extends PHPUnit_Framework_TestCase {

	protected $object;

	protected function setUp() {
		$this->object = new PDFDictionary();
	}

	public function test_creation_size() {
		// size should be 0 after creation
		$pdfDict = new PDFDictionary();
		$this->assertEquals(0, $pdfDict->size());
	}

	/**
	 * @covers PDFDictionary::remove
	 */
	public function testRemove() {
		// create and put an entrie
		$key = 'RemoveTest';
		$value = '/ABC';
		$this->object->put($key, $value);
		// does it realy exist?
		$this->assertEquals($value, $this->object->get($key));
		$size = $this->object->size();
		$this->object->remove($key);
		// test size and existance
		$this->assertEquals($size - 1, $this->object->size());
		$this->assertNull($this->object->get($key));
	}

	public function test_getId() {
		$pdfDictionary1 = new PDFDictionary();
		$pdfDictionary2 = new PDFDictionary();

		// a call of getId should generate a id for an object
		$id1 = $pdfDictionary1->getId();
		$this->assertEquals($id1, $pdfDictionary1->getId());
		// a call to getId of a second object should generate id1 + 1
		$id2 = $id1 + 1;
		$this->assertEquals($id2, $pdfDictionary2->getId());
	}

	public function test_getName_and_getLink() {
		$pdfDictionary = new PDFDictionary();
		$id = $pdfDictionary->getId();

		// name
		// generation number is assumed to be 0 all the time, caus we are createing new docs
		$expected = "$id 0";
		$this->assertEquals($expected, $pdfDictionary->getName());

		// link
		$expected .= ' R';
		$this->assertEquals($expected, $pdfDictionary->getLink());
		$this->assertEquals($expected, $pdfDictionary->getLink());
	}

	public function test_setType_setSubType() {
		$pdfDictionary = new PDFDictionary();

		// setType
		$expected = "<<\n" .
				"  /Type /Test\n";
		$pdfDictionary->setType('Test');
		$this->assertEquals($expected . ">>", $pdfDictionary->__toString());

		// setSubType
		$expected .= "  /SubType /SubTest\n";
		$pdfDictionary->setSubType('SubTest');
		$this->assertEquals($expected . ">>", $pdfDictionary->__toString());
	}

	/**
	 * @covers PDFDictionary::size
	 */
	public function testSize() {
		// empty after creation
		$pdfDictionary = new PDFDictionary();
		$this->assertEquals(0, $pdfDictionary->size());

		// put a mapping, size shuld inc by 1
		$pdfDictionary->put('Type', '/Test');
		$this->assertEquals(1, $pdfDictionary->size());
	}

	public function testGet() {
		$dict = new PDFDictionary();
		$dict->put('Type', '/Test1');
		$this->assertEquals('/Test1', $dict->get('Type'));
		$this->assertEquals(1, $dict->size());
		$dict->put('Type', '/Test2');
		$this->assertEquals('/Test2', $dict->get('Type'));
		$this->assertEquals(1, $dict->size());
	}

	public function test_put_get_and_toString() {
		// creation - empty dict
		$expected = "<<\n";
		$pdfDictionary = new PDFDictionary();
		$this->assertEquals($expected . '>>', $pdfDictionary->__toString());

		// put a string value
		$expected .= "  /Type /Test\n";
		$pdfDictionary->put('Type', '/Test');
		$this->assertEquals($expected . '>>', $pdfDictionary->__toString());

		// put an integer value
		$expected .= "  /Int 77\n";
		$pdfDictionary->put('Int', 77);
		$this->assertEquals($expected . '>>', $pdfDictionary->__toString());

		// put a float value
		$expected .= "  /Float " . pi() . "\n";
		$pdfDictionary->put('Float', pi());
		$this->assertEquals($expected . '>>', $pdfDictionary->__toString());

		// put a boolean false value
		$expected .= "  /Boolean false\n";
		$pdfDictionary->put('Boolean', false);
		$this->assertEquals($expected . '>>', $pdfDictionary->__toString());

		// put an integer 0 value
		$expected .= "  /Int0 0\n";
		$pdfDictionary->put('Int0', 0);
		$this->assertEquals($expected . '>>', $pdfDictionary->__toString());

		// put a float 0.0 value
		$expected .= "  /Float0 0\n";
		$pdfDictionary->put('Float0', 0.0);
		$this->assertEquals($expected . '>>', $pdfDictionary->__toString());

		// put a php reference of a dict
		$pdfDict2 = new PDFDictionary();
		$expected .= "  /Dict <<\n" .
				"  >>\n";
		$pdfDictionary->put('Dict', $pdfDict2);
		$this->assertEquals($expected . '>>', $pdfDictionary->__toString());

		// get
		$this->assertEquals($pdfDict2, $pdfDictionary->get('Dict'));
		$this->assertEquals(pi(), $pdfDictionary->get('Float'));
		$this->assertNull($pdfDictionary->get('ANotExistingKey'));
	}

	public function test_empty_keys_and_values() {
		$pdfDictionary = new PDFDictionary();
		$result = array();

		// empty keys
		$exceptionCached = false;
		try {
			$pdfDictionary->put('', '1 0');
		} catch (InvalidArgumentException $exc) {
			$exceptionCached = true;
		}
		if (!$exceptionCached)
			$result[] = 'empty keys';

		// null keys
		$exceptionCached = false;
		try {
			$pdfDictionary->put(null, '1 0');
		} catch (InvalidArgumentException $exc) {
			$exceptionCached = true;
		}
		if (!$exceptionCached)
			$result[] = 'null keys';

		// empty values
		$exceptionCached = false;
		try {
			$pdfDictionary->put('Test', '');
		} catch (InvalidArgumentException $exc) {
			$exceptionCached = true;
		}
		if (!$exceptionCached)
			$result[] = 'empty values';

		// null values
		$exceptionCached = false;
		try {
			$pdfDictionary->put('Test', null);
		} catch (InvalidArgumentException $exc) {
			$exceptionCached = true;
		}
		if (!$exceptionCached)
			$result[] = 'null values';

		if (count($result) > 0)
			$this->fail('An InvalidArgumentException has not been raised for:' . implode(', ', $result));

		// null objects
		$exceptionCached = false;
		try {
			$pdfDictionary->put('Test', '/');
		} catch (InvalidArgumentException $exc) {
			$exceptionCached = true;
		}
		if (!$exceptionCached)
		$result[] = '/ values';

		if (count($result) > 0)
		$this->fail('An InvalidArgumentException has not been raised for: ' . implode(', ', $result));
	}

	function testGetKeys() {
		$pdfDict = new PDFDictionary();
		// for an empty dict an emtpy keys array should be returned
		$keys = $pdfDict->keys();
		$this->assertTrue(is_array($keys));
		$this->assertEquals(0, count($keys));

		// put an entry
		$key = 'Type';
		$value = '/Test';
		$pdfDict->put($key, $value);
		$keys = $pdfDict->keys();
		$this->assertEquals(1, count($keys));
		$this->assertEquals($key, $keys[0]);
	}
}