<?php

require_once 'PHPUnit/Autoload.php';
require_once __DIR__ . '/../../../base/PDFArray.php';

class PDFArrayTest extends PHPUnit_Framework_TestCase {
	
    public function test_size() {
    	// empty
    	$pdfArray = new PDFArray();
    	$this->assertEquals(0, $pdfArray->size());
    	
    	// creation
    	$pdfArray = new PDFArray(1, 2, 3);
    	$this->assertEquals(3, $pdfArray->size());
    	
    	// add a value
    	$pdfArray->add(4);
    	$this->assertEquals(4, $pdfArray->size());
    }
    
    function testContains() {
    	$array = new PDFArray(77, '11 0 R');
    	$this->assertTrue($array->contains(77));
    	$this->assertTrue($array->contains('11 0 R'));
    }
    
    public function test_add_and_toString() {
    	// creation
    	$expected = '[';
    	$pdfArray = new PDFArray();
	    $this->assertEquals($expected . ' ]', $pdfArray->__toString());
		
	    // add a string
	    $expected .= ' /Test';
	    $pdfArray->add('/Test');
	    $this->assertEquals($expected . ' ]', $pdfArray->__toString());
	    
	    // add an integer
	    $expected .= ' 77';
	    $pdfArray->add(77);
	    $this->assertEquals($expected . ' ]', $pdfArray->__toString());
	     
	    // add a float
	    $expected .= ' ' . pi();
	    $pdfArray->add(pi());
	    $this->assertEquals($expected . ' ]', $pdfArray->__toString());
	     
	    // add a boolean value
	    $expected .= ' true';
	    $pdfArray->add(true);
	    $this->assertEquals($expected . ' ]', $pdfArray->__toString());
	    
	    // add an pdf dict
	    $dict = new PDFDictionary();
	    $dict->put('Type', '/Test');
	    $expected .= ' ' . $dict->__toString();
	    $pdfArray->add($dict);
	    $this->assertEquals($expected . ' ]', $pdfArray->__toString());
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function test_add_null() {
    	$pdfArray = new PDFArray();
    	$pdfArray->add(null);
    }

    /**
    * @expectedException InvalidArgumentException
    */
    public function test_add_empty_string() {
    	$pdfArray = new PDFArray();
    	$pdfArray->add('');
    }
    
    function testGet() {
    	$pdfArray = new PDFArray(1, 2, 3);
    	$this->assertEquals(1, $pdfArray->get(0));
    }
}