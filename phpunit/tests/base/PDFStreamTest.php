<?php

require_once 'PHPUnit/Autoload.php';
require_once __DIR__ . '/PDFDictionaryTest.php';
require_once __DIR__ . '/../../../base/PDFStream.php';

/**
 * A stream shall consist of a dictionary followed by zero or more bytes bracketed between the
 * keywords stream (followed by newline) and endstream. All streams shall be indirect objects
 * (see 7.3.10, "Indirect Objects") and the stream dictionary shall be a direct object.
 * The keyword stream that follows the stream dictionary shall be followed by an end-of-line marker
 * consisting of either a CARRIAGE RETURN and a LINE FEED or just a LINE FEED, and not by a
 * CARRIAGE RETURN alone. The sequence of bytes that make up a stream lie between the end-of-line
 * marker following the stream keyword and the endstream keyword; the stream dictionary specifies
 * the exact number of bytes. There should be an end-of-line marker after the data and before
 * endstream; this marker shall not be included in the stream length. There shall not be any extra
 * bytes, other than white space, between endstream and endobj.
 * Alternatively, beginning with PDF 1.2, the bytes may be contained in an external file, in which
 * case the stream dictionary specifies the file, and any bytes between stream and endstream
 * shall be ignored by a conforming reader.
 * 
 * @author chervbyn
 *
 */
class PDFStreamTest extends PDFDictionaryTest {

	function test_dictionary() {
		$pdfStream = new PDFStream();
		$pattern = '/^<<.*(\/Filter \/FlateDecode).*>>.*/s';
		$pdfStream->setFilter(PDFStream::FILTER_FLATEDECODE);
		$this->assertRegExp($pattern, $pdfStream->__toString(), 'Filter not set correctly.');
	}
	
	function test_empty_stream_and_append_and_toString() {
 		$pdfStream = new PDFStream();

 		// test end of line markers after stream and before endstream
 		$pattern = "/^<<.*\/Length 0.*>>.*stream(\n|\r\n){2}endstream.*/s";
 		$this->assertRegExp($pattern, $pdfStream->__toString());
 		
		// append string
 		$expected = 'abcdefghijklmnopqrstuvwxyz';
 		$pdfStream->append($expected);
 		// test getStream
 		$this->assertEquals($expected, $pdfStream->getStream());
 		// test __toString
 		$pattern = "/^<<.*\/Length " . strlen($expected). ".*>>.*stream(\n|\r\n){1}$expected(\n|\r\n){1}endstream.*/s";
 		$this->assertRegExp($pattern, $pdfStream->__toString());
 		
 		// append PDFSteam
 		$expected = 'abcdefghijklmnopqrstuvwxyz';
 		$pdfStream2 = new PDFStream();
 		$pdfStream2->append($expected);
 		$pdfStream = new PDFStream();
 		$pdfStream->append($pdfStream2);
 		// test getStream
 		$this->assertEquals($expected, $pdfStream->getStream());
 		// test __toString
 		$pattern = "/^<<.*\/Length " . strlen($expected). ".*>>.*stream(\n|\r\n){1}$expected(\n|\r\n){1}endstream.*/s";
 		$this->assertRegExp($pattern, $pdfStream->__toString());
 	}
 	
 	function test_setFilter_and_applyFilter() {
 		$pdfStream = new PDFStream();
 		$expected = 'abcdefghijklmnopqrstuvwxyz';
 		$pdfStream->append($expected);
 		
 		// setFilter
 		$pdfStream->setFilter(PDFStream::FILTER_FLATEDECODE);
 		$pattern = "/^<<.*\/Filter \/FlateDecode.*>>.*stream(\n|\r\n){1}$expected(\n|\r\n){1}endstream.*/s";
 		$this->assertRegExp($pattern, $pdfStream->__toString());
 		
 		// apply	 filter FlateDecode
 		$expected = gzcompress($expected);
 		$expLength = strlen($expected);
 		$pdfStream->applyFilter();
 		$pattern = '/^<<.*\/Length ' . $expLength . '.*>>.*stream(\n|\r\n){1}(.){' . $expLength . '}(\n|\r\n){1}endstream.*/s';
 		$this->assertRegExp($pattern, $pdfStream->__toString());
 	}
}