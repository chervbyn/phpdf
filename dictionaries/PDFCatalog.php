<?php

require_once __DIR__ . '/../base/PDFDictionary.php';
require_once __DIR__ . '/../base/PDFArray.php';
require_once __DIR__ . '/PDFPages.php';

/**
 * 7.7.2 Document Catalog
 * 
 * The root of a document’s object hierarchy is the catalog dictionary, located by means of the Root entry in the
 * trailer of the PDF file (see 7.5.5, "File Trailer"). The catalog contains references to other objects defining the
 * document’s contents, outline, article threads, named destinations, and other attributes. In addition, it contains
 * information about how the document shall be displayed on the screen, such as whether its outline and
 * thumbnail page images shall be displayed automatically and whether some location other than the first page
 * shall be shown when the document is opened. Table 28 shows the entries in the catalog dictionary.
 * 
 * @author chervbyn
 * @package pHPDF/dictionaries
 */
class PDFCatalog extends PDFDictionary {

	function __construct() {
		$this->put('Type', '/Catalog');
	}
	
	/**
	 * Set the page tree node that shall be the root of the document’s page tree.
	 * 
	 * (see 7.7.3, "Page Tree")
	 * 
	 * @param PDFPages $pages
	 */
	function setPages(PDFPages $pages) {
		$this->put('Pages', $pages->getLink());
	}
	
	/**
	 * Set the version of the PDF specification to which the document conforms (Optional; PDF 1.4).
	 * 
	 * For example: 1.4 - if later than the version
	 * specified in the file’s header (see 7.5.2, "File Header"). If the
 	 * header specifies a later version, or if this entry is absent, the
	 * document shall conform to the version specified in the header.
	 * This entry enables a conforming writer to update the version using
	 * an incremental update; see 7.5.6, "Incremental Updates."
	 * 
	 * @param double|string $version a version number
	 */
	function setVersion($version) {
		$this->put('Version', "/$version");
	}
	
	/**
	 * Set a language identifier that shall specify the natural language for all text in the document (Optional; PDF 1.4).
	 * 
	 * Except where overridden by language specifications for structure elements or
	 * marked content (see 14.9.2, "Natural Language Specification"). If
	 * this entry is absent, the language shall be considered unknown.
	 * 
	 * @param string $lang i.e.: en-US
	 */
	function setLanguage($lang) {
		$this->put('Lang', "($lang)");
	}
}