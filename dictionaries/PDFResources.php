<?php

require_once __DIR__ . '/../base/PDFDictionary.php';
require_once __DIR__ . '/../base/PDFArray.php';
require_once __DIR__ . '/PDFFont.php';
require_once __DIR__ . '/PDFPattern.php';
require_once __DIR__ . '/PDFExtGState.php';

/**
 * 
 * Enter description here ...
 * @author chervbyn
 * @package pHPDF/dictionaries
 */
class PDFResources extends PDFDictionary {
	
	function addFont($name, PDFFont $font) {
		$fonts = $this->get('Font');
		if (!$fonts) {
			$fonts = new PDFDictionary();
			$this->put('Font', $fonts);
		}
		
		$fonts->put($name, $font->getLink());
	}

	function addPattern($name, PDFPattern $pattern) {
		$patterns = $this->get('Pattern');
		if (!$patterns) {
			$patterns = new PDFDictionary();
			$this->put('Pattern', $patterns);
		}
		
		$patterns->put($name, $pattern->getLink());
	}
	
	function addExtGState($name, PDFExtGState $extGState) {
		$extGStates = $this->get('ExtGState');
		if (!$extGStates) {
			$extGStates = new PDFDictionary();
			$this->put('ExtGState', $extGStates);
		}
	
		$extGStates->put($name, $extGState->getLink());
	}
	
	function addXObject($name, PDFXObject $xobject) {
		$xobjects = $this->get('XObject');
		if (!$xobjects) {
			$xobjects = new PDFDictionary();
			$this->put('XObject', $xobjects);
		}
		$xobjects->put($name, $xobject->getLink());
	}
	
}