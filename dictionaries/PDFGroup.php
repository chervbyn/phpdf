<?php

require_once __DIR__ . '/../base/PDFDictionary.php';
require_once __DIR__ . '/../base/PDFArray.php';

/**
 * 
 * @author chervbyn
 * @package pHPDF/dictionaries
 */
class PDFGroup extends PDFDictionary {
	
	const SUBTYPE_TRANSPARENCY = 'Transparency';
	const CS_DEVICERGB = 'DeviceRGB';
	
	function __construct() {
		$this->setType('Group');
	}
	
	function setColorSpace($cs) {
		$this->put('CS', "/$cs");
	}
}