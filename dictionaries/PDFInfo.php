<?php

require_once __DIR__ . '/../base/PDFDictionary.php';
require_once __DIR__ . '/../base/PDFArray.php';

/**
 * 
 * Enter description here ...
 * @author chervbyn
 * @package pHPDF/dictionaries
 */
class PDFInfo extends PDFDictionary {
	
	const DATE_FORMAT = "YmdHis+2'00";
	
	const KEY_MODIFICATION_DATE = 'ModDate';
	
	function __construct() {
		$this->setCreationDate();
		$this->setCreator('pHPDF');
	}
	
	/**
	 * (Optional) The date and time the document was created, in human-
	 * readable form (see 7.9.4, “Dates”).
	 * 
	 * Sets the current time, if timestamp parameter is omitted.
	 * 
	 * @param unknown_type $timestamp
	 */
	function setCreationDate($timestamp = null) {
		if ($timestamp === null)
			$timestamp = time();

		$this->put('CreationDate', '(D:' . date(self::DATE_FORMAT, $timestamp) . ')');
	}
	
	/**
	 * Required if PieceInfo is present in the document catalogue;
	 * otherwise optional; PDF 1.1) The date and time the document was
	 * most recently modified, in human-readable form (see 7.9.4, “Dates”).
	 * 
	 * Sets the current time, if timestamp parameter is omitted.
	 * 
	 * @param int $timestamp
	 */
	function setModificationDate($timestamp = null) {
		if ($timestamp === null)
			$timestamp = time();

		$this->put('ModDate', '(D:' . date(self::DATE_FORMAT, $timestamp) . ')');
	}
	
	function setSubject($subject) {
		$this->put('Subject', "($subject)");
	}
	
	/**
	 * Set author info.
	 * 
	 * @param string $author
	 */
	function setAuthor($author) {
		$this->put('Author', "($author)");
	}
	
	function setCreator($creator) {
		$this->put('Creator', "($creator)");
	}
	
	function setProducer($producer) {
		$this->put('Producer', "($producer)");
	}
	
	function setTitle($title) {
		$this->put('Title', "($title)");
	}
}