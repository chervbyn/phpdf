<?php

require_once __DIR__ . '/../base/PDFDictionary.php';
require_once __DIR__ . '/../base/PDFArray.php';
//require_once __DIR__ . '/PDFPages.php';

/**
 * 
 * Enter description here ...
 * @author chervbyn
 * @package pHPDF/dictionaries
 */
class PDFFont extends PDFDictionary {
	
	const TRUE_TYPE = 'TrueType';
	const TYPE_0 = 'Type0';
	const TYPE_1 = 'Type1';
	const TYPE_3 = 'Type3';
	
	function __construct($baseFont, $subType) {
		$this->setType('Font');
		$this->setSubType($subType);
		$this->put('BaseFont', "/$baseFont");
// 		$this->put('FontBBox', '[ 0 0 750 750 ]');
// 		$this->put('FontMatrix', '[ 0.001 0 0 0.001 0 0 ]');
	}
}
