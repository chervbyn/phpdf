<?php

require_once __DIR__ . '/../base/PDFDictionary.php';
require_once __DIR__ . '/../base/PDFArray.php';

/**
 * 
 * Enter description here ...
 * @author chervbyn
 * @package pHPDF/dictionaries
 */
class PDFFunction extends PDFDictionary {
	
	const SAMPELED = 0;
	const EXPONENTIAL_INTERPOLATION = 2;
	const STITCHING = 3;
	const POSTSCRIPT_CALCULATOR = 4;
	
	function __construct($type) {
		$this->put('FunctionType', $type);
		// example data
// 		$this->put('Domain', new PDFArray(0, 1));
// 		$this->put('C0', new PDFArray(0, 0, 1));
// 		$this->put('C1', new PDFArray(0.5, 0, 0));
// 		$this->put('N', 1);
	}
}