<?php

require_once __DIR__ . '/PDFPageTreeNode.php';
require_once __DIR__ . '/PDFPages.php';
require_once __DIR__ . '/PDFResources.php';
require_once __DIR__ . '/PDFInfo.php';

require_once __DIR__ . '/../base/PDFDictionary.php';
require_once __DIR__ . '/../base/PDFArray.php';
require_once __DIR__ . '/../base/PDF.php';

/**
 * The page dictionary.
 *
 * The leaves of the page tree are page objects, each of which is a dictionary specifying the attributes of a single
 * page of the document. Table 30 shows the contents of this dictionary. The table also identifies which attributes
 * a page may inherit from its ancestor nodes in the page tree, as described under 7.7.3.4, "Inheritance of Page
 * Attributes." Attributes that are not explicitly identified in the table as inheritable shall not be inherited.
 *
 * @author chervbyn
 * @package pHPDF/dictionaries
 */
class PDFPage extends PDFPageTreeNode {

	protected $resources;
	protected $mainContentStream;
	protected $imgCounter = 1;
	protected $width = 596.59;
	protected $height = 842.55;

	function __construct(PDF $pdf) {
		parent::__construct($pdf);
		$this->setType('Page');
		$this->resources = new PDFResources();
		$this->setResources($this->resources);
		$this->mainContentStream = new PDFContent();
		$this->addContent($this->mainContentStream);
	}

	function addCanvas(PDFCanvas $pdfCanvas = null) {
		if (!$pdfCanvas) {
			$pdfCanvas = new PDFCanvas();
		}
		$this->mainContentStream->append($pdfCanvas);
		return $pdfCanvas;
	}

	function addText(PDFText $pdfText = null) {
		if (!$pdfText) {
			$pdfText = new PDFText();
		}
		$this->mainContentStream->append($pdfText);
		return $pdfText;
	}

	/**
	 * Add a content stream that shall describe the contents of this page.
	 *
	 * (see 7.8.2, "Content Streams")
	 *
	 * @param PDFContent $content
	 */
	function addContent(PDFContent $content) {
		$contents = $this->get('Contents');
		if (!$contents) {
			$contents = new PDFArray();
			$this->put('Contents', $contents);
		}
		$contents->add($content->getLink());
		$this->pdf->addObject($content);
	}

	function getContents() {
		return $this->get('Contents');
	}

	/**
	 * Set a dictionary containing any resources required by the page. (Required; inheritable)
	 *
	 * (see 7.8.3, "Resource Dictionaries"). If the page requires no resources, the value of this
	 * entry shall be an empty dictionary. Omitting the entry entirely indicates that the resources
	 * shall be inherited from an ancestor node in the page tree.

	 * @param PDFResources $resources
	 */
	function setResources(PDFResources $resources) {
		$this->put('Resources', $resources);
	}

	/**
	 * Get the resource dict of this page.
	 *
	 * @return PDFResources
	 */
	function getResources() {
		return $this->get('Resources');
	}

	/**
	 * Set the date and time when the page’s contents were most recently modified.
	 * (Required if PieceInfo is present; optional otherwise; PDF 1.3)
	 *
	 * (see 7.9.4, "Dates"). If a page-piece dictionary (PieceInfo) is present, the modification
	 * date shall be used to ascertain which of the application data dictionaries that it contains
	 * correspond to the current content of the page (see 14.5, "Page-Piece Dictionaries").

	 * @param int $timestamp
	 */
	function setLastModified($timestamp) {
		$this->put('LastModified', '(D:' . date(PDFInfo::DATE_FORMAT, $timestamp) . ')');

	}

	/**
	 * Sets the size of a user space unit.
	 *
	 * A positive number that shall give the size of default user space units, in multiples of
	 * 1 ⁄ 72 inch. The range of supported values shall be implementation-dependent.
	 *
	 * Default value: 1.0 (user space unit is 1 ⁄ 72 inch).
	 *
	 * @param float $userUnit
	 */
	function setUserUnit($userUnit) {
		$this->put('UserUnit', $userUnit);
	}

	function addImage($file, $x, $y, $width = null, $height = null) {
		$xo = ImageParser::parseFile($file);
		if ($xo) {
			if ($width === null && $height === null) {
				$width = $xo->get('Width');
				$height = $xo->get('Height');
			} else if ($width === null) {
				// fixed width
				$width = ($height / $xo->get('Height')) * $xo->get('Width');
			} else if ($height === null) {
				// fixed height
				$height = ($width / $xo->get('Width')) * $xo->get('Height');
			}

			$yMirror = $this->height - $y - $height;

			$this->getResources()->addXObject('IMG' . $this->imgCounter, $xo);
			$this->pdf->addObject($xo);
			$this->mainContentStream->append("q $width 0 0 $height $x {$yMirror} cm /IMG{$this->imgCounter} Do Q ");
			$this->imgCounter++;
		}
		return $xo;
	}

}
