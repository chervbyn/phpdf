<?php

require_once __DIR__ . '/../base/PDFDictionary.php';
require_once __DIR__ . '/../base/PDFArray.php';

require_once __DIR__ . '/PDFCatalog.php';

/**
 * 
 * Enter description here ...
 * @author chervbyn
 * @package pHPDF/dictionaries
 */
class PDFTrailer extends PDFDictionary {
	
	function setRoot(PDFCatalog $catalog) {
		$this->put('Root', $catalog->getLink());
	}
	
	/**
	 * 
	 * @param int $size number of objects
	 */
	function setSize($size) {
		$this->put('Size', $size);
	}
	
	function setInfo(PDFInfo $info) {
		$this->put('Info', $info->getLink());
	}
}