<?php

require_once __DIR__ . '/../base/PDFDictionary.php';
require_once __DIR__ . '/../base/PDFArray.php';
require_once __DIR__ . '/PDFShading.php';

/**
 * 
 * Enter description here ...
 * @author chervbyn
 * @package pHPDF/dictionaries
 */
class PDFPattern extends PDFDictionary {

	const TILING_PATTERN = 1;
	const SHADING_PATTERN = 2;
	
	function __construct($type) {
		$this->setType('Pattern');
		$this->setPatternType($type);
		// example data
		$this->put('Matrix', '[ 1 0 0 -1 0 500 ]');
	}
	
	function setShading(PDFShading $shading) {
		$this->put('Shading', $shading);
	}
	
	function setPatternType($patternType) {
		$this->put('PatternType', $patternType);
	}
	
}

