<?php

require_once __DIR__ . '/../base/PDFDictionary.php';
require_once __DIR__ . '/../base/PDFArray.php';
require_once __DIR__ . '/PDFFunction.php';

/**
 * 
 * Enter description here ...
 * @author chervbyn
 * @package pHPDF/dictionaries
 */
class PDFShading extends PDFDictionary {
	
	const FUNCTION_BASED = 1;
	const AXIAL_SHADING = 2;
	const RADIAL_SHADING = 3;
	const FREE_FORM = 4;
	const LATTICE_FORM = 5;
	const COONS_PATCH = 6;
	const TENSOR_PRODUCT_PATCH = 7;	
	
	function __construct($type) {
		$this->put('ShadingType', $type);
		// example data
		/*
		$this->put('ColorSpace', '/DeviceRGB');
		$this->put('Coords', new PDFArray(0, 0, 100, 100));
		$this->put('Domain', new PDFArray(0, 1));
		$this->put('Extend', new PDFArray('true', 'true'));
		*/
	}
	
	function setFunction(PDFFunction $function) {
		$this->put('Function', $function->getLink());
	}
}