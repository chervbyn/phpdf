<?php

require_once __DIR__ . '/../base/PDFDictionary.php';
require_once __DIR__ . '/../base/PDFArray.php';

/**
 * 
 * Enter description here ...
 * @author chervbyn
 * @package pHPDF/dictionaries
 */
class PDFExtGState extends PDFDictionary {
	
	const LINE_BUTT_CAP = 0;
	const LINE_ROUND_CAP = 1;
	const LINE_PROJECTING_SQUARE_CAP = 3;
	
	const LINE_MITER_JOIN = 0;
	const LINE_ROUNT_JOIN = 1;
	const LINE_BEVEL_JOIN = 2;
	
	function __construct() {
		$this->put('Type', '/ExtGState');
	}
	
	/**
	 * (Optional; PDF 1.4) The current stroking alpha constant, specifying the
	 * constant shape or constant opacity value that shall be used for
	 * stroking operations in the transparent imaging model (see 11.3.7.2,
	 * "Source Shape and Opacity" and 11.6.4.4, "Constant Shape and
	 * Opacity").
	 * 
	 * @param unknown_type $alpha
	 */
	function setStrokingAlpha($alpha) {
		$this->put('CA', $alpha);
	}
	
	/**
	 * Optional; PDF 1.4) Same as CA, but for nonstroking operations.
	 * 
	 * @param unknown_type $alpha
	 */
	function setNonStrokingAlpha($alpha) {
		$this->put('ca', $alpha);
	}
	
	/**
	 * (Optional; PDF 1.3) The line width (see 8.4.3.2, "Line Width").
	 * 
	 * @param float $lineWidth
	 */
	function setLineWidth($lineWidth) {
		$this->put('LW', $lineWidth);
	}
	
	/**
	 * (Optional; PDF 1.3) The line cap style (see 8.4.3.3, "Line Cap Style").
	 * 
	 * @param int $lineCap
	 */
	function setLineCap($lineCap) {
		$this->put('LC', $lineCap);
	}
	/**
	 * (Optional; PDF 1.3) The line join style (see 8.4.3.4, "Line Join Style").
	 * 
	 * @param int $lineJoin
	 */
	function setLineJoin($lineJoin) {
		$this->put('LJ', $lineJoin);
	}
}
