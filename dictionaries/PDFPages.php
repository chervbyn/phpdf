<?php

require_once __DIR__ . '/PDFPage.php';
require_once __DIR__ . '/PDFPageTreeNode.php';
require_once __DIR__ . '/../base/PDFDictionary.php';
require_once __DIR__ . '/../base/PDFArray.php';

/**
 *
 * Enter description here ...
 * @author chervbyn
 * @package pHPDF/dictionaries
 */
class PDFPages extends PDFPageTreeNode {

	protected $pages = array();

	function __construct(PDF $pdf) {
		parent::__construct($pdf);
		$this->setType('Pages');
		$this->put('Kids', new PDFArray());
		$this->put('Count', 0);
		$this->setMediaBox(0, 0, 596.59, 842.55);
	}

	function addKid(PDFPageTreeNode $node) {
		$node->setParent($this);
		if (!in_array($node, $this->pages)) {
			if ($node instanceof PDFPage) {
				$this->pages[] = $node;
				$this->put('Count', count($this->pages));
			}
			$kids = $this->get('Kids');
			$kids->add($node->getLink());
		}
	}

	/**
	 *
	 * @return PDFArray
	 */
	function getKids() {
		return $this->get('Kids');
	}

	function getPage($index) {
		return $this->pages[$index];
	}
}