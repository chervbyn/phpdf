<?php

require_once __DIR__ . '/../base/PDFDictionary.php';
require_once __DIR__ . '/../base/PDFArray.php';
require_once __DIR__ . '/PDFPage.php';

class PDFPageTreeNode extends PDFDictionary {

	protected $pdf;

	function __construct(PDF $pdf) {
		$this->pdf = $pdf;
	}

	/**
	* Set page's media box (Required; inheritable).
	*
	* A rectangle (see 7.9.5, "Rectangles"), expressed in default user space units, that shall
	* define the boundaries of the physical medium on which the page shall be displayed or
	* printed (see 14.11.2, "Page Boundaries").
	*
	* @param int $x1 lower left x
	* @param int $y1 lower left y
	* @param int $x2 upper right x
	* @param int $y2 upper right y
	*/
	function setMediaBox($x1, $y1, $x2, $y2) {
		$this->put('MediaBox', new PDFArray($x1, $y1, $x2, $y2));
	}

	/**
	 * Set page's crop box (Optional; inheritable).
	 *
	 * A rectangle, expressed in default user space units, that shall define the visible region of
	 * default user space. When the page is displayed or printed, its contents shall be clipped
	 * (cropped) to this rectangle and then shall be imposed on the output medium in some
	 * implementation-defined manner (see 14.11.2, "Page Boundaries").
	 *
	 * Default value: the value of MediaBox.
	 *
	 * @param int $x1 lower left x
	 * @param int $y1 lower left y
	 * @param int $x2 upper right x
	 * @param int $y2 upper right y
	 */
	function setCropBox($x1, $y1, $x2, $y2) {
		$this->put('CropBox', new PDFArray($x1, $y1, $x2, $y2));
	}

	/**
	 * Set page's bleed box (Optional; PDF 1.3).
	 *
	 * A rectangle, expressed in default user space units, that shall define the region to which
	 * the contents of the page shall be clipped when output in a production environment
	 * (see 14.11.2, "Page Boundaries").
	 *
	 * Default value: the value of CropBox.
	 *
	 * @param int $x1 lower left x
	 * @param int $y1 lower left y
	 * @param int $x2 upper right x
	 * @param int $y2 upper right y
	 */
	function setBleedBox($x1, $y1, $x2, $y2) {
		$this->put('BleedBox', new PDFArray($x1, $y1, $x2, $y2));
	}

	/**
	 * Set page's trim box (Optional; PDF 1.3).
	 *
	 * A rectangle, expressed in default user space units, that shall define the intended dimensions
	 * of the finished page after trimming (see 14.11.2, "Page Boundaries").
	 *
	 * Default value: the value of CropBox.
	 *
	 * @param int $x1 lower left x
	 * @param int $y1 lower left y
	 * @param int $x2 upper right x
	 * @param int $y2 upper right y
	 */
	function setTrimBox($x1, $y1, $x2, $y2) {
		$this->put('TrimBox', new PDFArray($x1, $y1, $x2, $y2));
	}

	/**
	 * Set page's art box (Optional; PDF 1.3).
	 *
	 * A rectangle, expressed in default user space units, that shall define the extent of the
	 * page’s meaningful content (including potential white space) as intended by the page’s
	 * creator (see 14.11.2, "Page Boundaries").
	 *
	 * Default value: the value of CropBox.
	 *
	 * @param int $x1 lower left x
	 * @param int $y1 lower left y
	 * @param int $x2 upper right x
	 * @param int $y2 upper right y
	 */
	function setArtBox($x1, $y1, $x2, $y2) {
		$this->put('ArtBox', new PDFArray($x1, $y1, $x2, $y2));
	}

	/**
	 * Set the parent page tree node. (Required)
	 *
	 * That is the immediate parent of this page object.
	 *
	 * @param PDFPages $pages
	 */
	function setParent(PDFPages $pages) {
		$this->put('Parent', $pages->getLink());
	}
}