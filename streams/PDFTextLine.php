<?php

require_once __DIR__ . '/../base/PDFStream.php';

class PDFTextLine extends PDFStream {

  protected $text = '';
  protected $x, $y;
  
  function __construct($text, $x = 0, $y = 0) {
    $this->text = $text;
    $this->setOrigin($x, $y);
  }
  
  function setOrigin($x, $y) {
    $this->x = $x;
    $this->y = $y;
  }
    
  function getStream() {
    $parentStream = parent::getStream();

    if ($this->x || $this->y)
      $stream[] = "{$this->x} {$this->y} Td";

    $stream[] = " ($this->text) Tj";
    $stream[] = $parentStream;
    
    if ($parentStream)
      $stream[] = $parentStream;
    
    return implode(' ', $stream);
  }
}