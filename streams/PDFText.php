<?php

require_once __DIR__ . '/../content/PDFContent.php';

class PDFTextBox extends PDFContent {

  const MODE_FILL = 0;
  const MODE_STROKE = 1;
  const MODE_FILL_STROKE = 2;
  const MODE_INVISIBLE = 3;
  const MODE_FILL_CLIPPING = 4;
  const MODE_STROKE_CLIPPING = 5;
  const MODE_FILL_STROKE_CLIPPING = 6;
  const MODE_CLIPPING = 7;
  
  protected $lines = array();
  protected $font = 'FONTArial';
  protected $fontSize = 12;
  protected $x, $y;
  protected $renderingMode = self::MODE_FILL;
  
  function __construct($x = 0, $y = 0) {
    $this->x = $x;
    $this->y = $y;
  }
  
  function addLine($line) {
    $this->lines[] = $line;
  }
  
  function addText($text) {
    // TODO manage unix/windows ends
    $lines = split("\n", $text);
    foreach ($lines as $l) {
      $this->addLine(new PDFTextLine($l));
    }
  }
  
  function setOrigin($x, $y) {
    $this->x = $x;
    $this->y = $y;
  }
  
  function setFontSize($fontSize) {
    $this->fontSize = $fontSize;
  }
  
  function setRenderingMode($mode) {
    $this->renderingMode = $mode;
  }
  
  function getStream() {
    $parentStream = parent::getStream();
    $stream = array(' BT');
    
    if ($this->font)
      $stream[] = "/{$this->font}";
    
    if ($this->fontSize)
      $stream[] = "{$this->fontSize} Tf";

    if ($this->x || $this->y)
      $stream[] = "{$this->x} {$this->y} Td";
    
    if ($this->renderingMode)
      $stream[] = " {$this->renderingMode} Tr";     
    
    for ($i = count($this->lines) - 1; $i >= 0; $i--) {
      $line = $this->lines[$i];
      $line->setOrigin(0, $this->fontSize);
      $stream[] = $line->getStream();
    }

    $stream[] = " ET";
    
    if ($parentStream)
      $stream[] = $parentStream;
    
    return implode(' ', $stream);
    
  }
}