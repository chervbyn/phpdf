<?php

require_once __DIR__ . '/../base/PDFStream.php';

class PDFPath extends PDFStream {

  /**
   * Begin a new subpath by moving the current point to
   * coordinates (x, y), omitting any connecting line segment. If
   * the previous path construction operator in the current path
   * was also m, the new m overrides it; no vestige of the
   * previous m operation remains in the path.
   * 
   * @param float $x
   * @param float $y
   */
  function move($x, $y) {
    $this->append(" $x $y m");
  }
  
  /**
   * Append a straight line segment from the current point to the
   * point (x, y). The new current point shall be (x, y).
   * 
   * @param unknown $x
   * @param unknown $y
   */
  function line($x, $y) {
    $this->append(" $x $y l");
  }
  
  /**
   * Append a cubic Bézier curve to the current path. The curve
   * shall extend from the current point to the point (x3 , y3 ), using
   * (x1 , y1 ) and (x2 , y2 ) as the Bézier control points (see 8.5.2.2,
   * "Cubic Bézier Curves"). The new current point shall be
   * (x3 , y3 ).
   * 
   * @param unknown $x1
   * @param unknown $y1
   * @param unknown $x2
   * @param unknown $y2
   * @param unknown $x3
   * @param unknown $y3
   */
  function curve($x1, $y1, $x2, $y2, $x3, $y3) {
    $this->append(" $x1 $y1 $x2 $y2 $x3 $y3 c");
  }
  
  /**
   * Append a cubic Bézier curve to the current path. The curve
   * shall extend from the current point to the point (x3 , y3 ), using
   * (x1 , y1 ) and (x3 , y3 ) as the Bézier control points (see 8.5.2.2,
   * "Cubic Bézier Curves"). The new current point shall be
   * (x3 , y3 ).
   * 
   * @param unknown $x1
   * @param unknown $y1
   * @param unknown $x3
   * @param unknown $y3
   */
  function curve1($x1, $y1, $x3, $y3) {
    $this->append(" $x1 $y1 $x3 $y3 y");
  }
  
  /**
   * Append a cubic Bézier curve to the current path. The curve
   * shall extend from the current point to the point (x3 , y3 ), using
   * the current point and (x2 , y2 ) as the Bézier control points (see
   * 8.5.2.2, "Cubic Bézier Curves"). The new current point shall
   * be (x3 , y3 ).
   * 
   * @param unknown $x2
   * @param unknown $y2
   * @param unknown $x3
   * @param unknown $y3
   */
  function curve2($x2, $y2, $x3, $y3) {
    $this->append(" $x2 $y2 $x3 $y3 v");
  }
  
  /**
   * Close the current subpath by appending a straight line
   * segment from the current point to the starting point of the
   * subpath. If the current subpath is already closed, h shall do
   * nothing.
   * This operator terminates the current subpath. Appending
   * another segment to the current path shall begin a new
   * subpath, even if the new segment begins at the endpoint
   * reached by the h operation.
   * 
   */
  function close() {
    $this->append(' h');
  }
  
  /**
   * Append a rectangle to the current path as a complete
   * subpath, with lower-left corner (x, y) and dimensions width
   * and height in user space.
   * 
   * @param unknown $x
   * @param unknown $y
   * @param unknown $width
   * @param unknown $height
   */
  function rect($x, $y, $width, $height) {
    $this->append(" $x $y $width $height re");
  }
  
  /**
   * Stroke the path.
   */
  function stroke() {
    $this->append(' S');
  }

  /**
   * Close and stroke the path. This operator shall have the same effect as the
   * sequence h S.
   */
  function strokeClose() {
    $this->append(' s');
  }
  
  /**
   * Fill the path.
   * 
   * @param string $evenOdd If true nonzero winding number rule else even odd rule
   */
  function fill($evenOdd = false) {
    if ($evenOdd)
      $this->append(' f*');
    else
      $this->append(' f');
  }
  
  /**
   * Fill and then stroke the path.
   * 
   * @param string $evenOdd If true nonzero winding number rule else even odd rule.
   */
  function fillStroke($evenOdd = false) {
    if ($evenOdd)
      $this->append(' B*');
    else
      $this->append(' B');
  }
  
  /**
   * Close fill and stroke the path.
   * 
   * @param string $evenOdd If true nonzero winding number rule else even odd rule.
   */
  function fillStrokeClose($evenOdd = false) {
    if ($evenOdd)
      $this->append(' b*');
    else
      $this->append(' b');
  }
  
  /**
   * End the path object without filling or stroking it. This operator shall be a path-
   * painting no-op, used primarily for the side effect of changing the current
   * clipping path (see 8.5.4, "Clipping Path Operators").
   */
  function end() {
    $this->append(' n');
  }
}