<?php

require_once __DIR__ . '/PDFDictionary.php';

/**
 * A PDF stream object is a dictionary followed by a stream
 * off data. 
 * 
 * 7.3.8 
 * 
 * @author chervbyn
 * @package pHPDF/base
 */
class PDFStream extends PDFDictionary {
	
	const FILTER_FLATEDECODE = 'FlateDecode';
	const FILTER_DCTDECODE = 'DCTDecode';
	const FILTER_ASCII85DECODE = 'ASCII85Decode'; 
	
	protected $content = array();
	protected $applyFilter = false;
	protected $filterName;
	
	/**
	 * Set the filter .
	 * 
	 * @param string $filterName
	 */
	function setFilter($filterName) {
 		$this->filterName = $filterName;
		$this->put('Filter', "/$filterName");
	}
	
	function applyFilter($apply = true) {
		$this->applyFilter = $apply;
	}
	
	/**
	 * Returns the stream's (unfiltered) content.
	 * 
	 * @return string
	 */
	function getStream() {
		$stream = '';
		foreach ($this->content as $content) {
			if ($content instanceof PDFStream) {
				$stream .= $content->getStream();
			} else {
				$stream .= $content;
			}
		}
		return $stream;
	}

	/**
	 * @param PDFStream|String $content
	 */
	function append($content) {
		if (!$content instanceof PDFStream)
			$content = "$content";

		$this->content[] = $content;
	}
	
	function __toString() {
		$stream = $this->getStream();
		// apply filter
		if ($this->applyFilter && $this->filterName) {
			$stream = $this->encode($this->filterName, $stream);
		}
		// put length
		$this->put('Length', strlen($stream));
		// render dict
		$streamPDF = parent::__toString() . "\n";
		// render stream
		$padding = str_pad('', self::$intent * 2, ' ');
		$streamPDF .= "stream\n";
		$streamPDF .= $stream;
		$streamPDF .= "\nendstream";
		return $streamPDF;
	}
	
	protected function encode($filterName, $data) {
		$encodedData = $data;
		switch ($filterName) {
			case self::FILTER_FLATEDECODE:
				$encodedData = gzcompress($data);
			break;
		}
		return $encodedData;
	}
}