<?php
/**
 * A PDF style array.
 * 
 * @author chervbyn
 * @package pHPDF
 * @subpackage base
 */
class PDFArray {
	protected  $values = array();

	function __construct() {
		$args = func_get_args();
		foreach ($args as $index => $item) {
			$this->add($item);
		}
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @param unknown_type $value
	 * @throws InvalidArgumentException If an empty value is 
	 */
	function add($value) {
		if ($value === null || $value === '')
			throw new InvalidArgumentException("Adding empty strings is not allowed.");

		if (gettype($value) == 'boolean')
			$value = $value ? 'true' : 'false';
		
		$this->values[] = $value;
	}
	
	function get($index) {
		return $this->values[$index];
	}
	
	function size() {
		return count($this->values);
	}
	
	function contains($value) {
		return in_array($value, $this->values);
	}
	
	function __toString() {
		$arrayPDF = '[';
		$list = implode(' ', $this->values);
		if ($list)
			$arrayPDF .= " $list";
		
		$arrayPDF .= ' ]';
		return $arrayPDF;
	}
}