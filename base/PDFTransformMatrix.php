<?php

require_once __DIR__ . '/PDFArray.php';

class PDFTransformMatrix extends PDFArray {
	
	function __construct() {
		parent::__construct();
	}
	
	function translate($x, $y) {
		
	}
	
	function scale($x, $y) {
		
	}
	
	function rotate($deg) {
		
	}
	
	function skew($a, $b) {
		
	}
}