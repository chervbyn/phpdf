<?php

/**
 * A dictionary object is an associative table containing pairs of objects, known as the
 * dictionary’s entries.
 * 
 * Dictionary objects are the main building blocks of a PDF document. They are commonly used to
 * collect and tie together the attributes of a complex object, such as a font or a page of the
 * document, with each entry in the dictionary specifying the name and value of an attribute. By
 * convention, the Type entry of such a dictionary, if present, identifies the type of object the
 * dictionary describes. In some cases, a Subtype entry (sometimes abbreviated S) may be used to
 * further identify a specialized subcategory of the general type. The value of the Type or Subtype
 * entry shall always be a name. For example, in a font dictionary, the value of the Type entry
 * shall always be Font, whereas that of the Subtype entry may be Type1, TrueType, or one of
 * several other values.
 * The value of the Type entry can almost always be inferred from context. The value of an entry in
 * a page's font resource dictionary, for example, shall be a font object; therefore, the Type
 * entry in a font dictionary serves primarily as documentation and as information for error
 * checking. The Type entry shall not be required unless so stated in its description; however,
 * if the entry is present, it shall have the correct value. In addition, the value of the Type
 * entry in any dictionary, even in private data, shall be either a name defined in this standard
 * or a registered name; see Annex E for details.
 * 
 * @author chervbyn
 * @package pHPDF/base
 */
class PDFDictionary {
	
	/**
	 * the map
	 * 
	 * @var array
	 */
	protected $values = array();

	/** 
	 * A positive integer called object number.
	 * 
	 * Indirect objects may be numbered sequentially within a PDF file, but this is not required;
	 * object numbers may be assigned in any arbitrary order.
	 * 
	 * @var int
	 */
	protected $id = 0;

	/**
	 * A non-negative integer generation number.
	 * 
	 * In a newly created file, all indirect objects shall have generation numbers of 0. Nonzero
	 * generation numbers may be introduced when the file is later updated; see sub-clauses 7.5.4,
	 * "Cross-Reference Table" and 7.5.6, "Incremental Updates."
	 * 
	 * @var int
	 */
	protected $generationNumber = 0;
	
	protected static $intent;
	protected static $objectCount;
	protected $type;
	protected $subType;
	
	/**
	 * Get the object's name.
	 * 
	 * It is a conncatenation of object number and generation number.
	 * 
	 * @return string the object's name
	 */
	function getName() {
		return $this->getId() . ' ' . $this->generationNumber;
	}
	
	/**
	 * Get the indirect reference for this object.
	 * 
	 * The object may be referred to from elsewhere in the file by an indirect reference. Such
	 * indirect references shall consist of the object number, the generation number, and the
	 * keyword R (with white space separating each part).
	 * 
	 * i.e.: 12 0 R
	 * 
	 * @return string the indirect reference to this object
	 */
	function getLink() {
		return $this->getName() . ' R';
	}

	/**
	 * Get the object number.
	 * 
	 * If this method is called the first time on an object, a unique object number is created
	 * for it.
	 * 
	 * @return int the object number
	 */
	function getId() {
		if ($this->id < 1) {
			self::$objectCount++;
			$this->id = self::$objectCount;
		}
		return $this->id;
	}
	
	/**
	 * Set the dictionary's Type.
	 * 
	 * By convention, the Type entry of such a dictionary, if present, identifies the type of
	 * object the dictionary describes.
	 * 
	 * @param string $type
	 */
	function setType($type) {
		$this->type = $type;
		$this->put('Type', "/$type");
	}
	
	/**
	 * Set the dictionary's Subtype.
	 * 
	 * In some cases, a Subtype entry (sometimes abbreviated S) may be used to further identify a
	 * specialized subcategory of the general type.
	 * 
	 * @param string $subType
	 */
	function setSubType($subType) {
		$this->subType = $subType;
		$this->put('SubType', "/$subType");
	}
	
	/**
	 * Add a new entry.
	 * 
	 * An entry is a key-value-pair. Values can be strings, numbers
	 * 
	 * @param string $key
	 * @param mixed $value
	 * @throws InvalidArgumentException if key or value is an empty string or null
	 */
	function put($key, $value) {
		$key = trim($key);
		if ($key === null || $key === '')
			throw new InvalidArgumentException('An empty string or null as key is not allowed.');

		if (is_bool($value)) {
			$value = $value ? 'true' : 'false';
		} else if (is_numeric($value)) {
			
		} else if (trim($value) == '/' || trim($value) == null) {
 			throw new InvalidArgumentException('An empty string or null as value is not allowed.');
		}

		$this->values[$key] = $value;
	}

	/**
	 * Returns the value for a key
	 * 
	 * Or null, if the key is not set.
	 * 
	 * @param string $key the key
	 * @return mixed the value or null
	 */
	function get($key) {
		$value = null;
		if (array_key_exists($key, $this->values))
			$value = $this->values[$key];
		
		return $value;
	}
	
	/**
	 * Count entries.
	 * 
	 * Get the number of contained objects.
	 * 
	 * @return int the size of this dictionary
	 */
	function size() {
		return count($this->values);
	}
	
	function keys() {
		return array_keys($this->values);
	}
	
	function remove($key) {
		unset($this->values[$key]);
	}
	
	function __toString() {
		$padding = str_pad('', self::$intent * 2, ' ');
		self::$intent++;
		$dictPDF = "<<\n";
		foreach ($this->values as $key => $value) {
			$dictPDF .= "$padding  /$key $value\n";
		}
		$dictPDF .= "$padding>>";
		self::$intent--;
		return $dictPDF;
	}
}