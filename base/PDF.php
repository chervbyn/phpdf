<?php

require_once __DIR__ . '/PDFArray.php';
require_once __DIR__ . '/PDFDictionary.php';
require_once __DIR__ . '/PDFStream.php';

require_once __DIR__ . '/../dictionaries/PDFFont.php';
require_once __DIR__ . '/../dictionaries/PDFCatalog.php';
require_once __DIR__ . '/../dictionaries/PDFPage.php';
require_once __DIR__ . '/../dictionaries/PDFPages.php';
require_once __DIR__ . '/../dictionaries/PDFResources.php';
require_once __DIR__ . '/../dictionaries/PDFInfo.php';
require_once __DIR__ . '/../dictionaries/PDFGroup.php';
require_once __DIR__ . '/../dictionaries/PDFExtGState.php';
require_once __DIR__ . '/../dictionaries/PDFFunction.php';
require_once __DIR__ . '/../dictionaries/PDFPattern.php';
require_once __DIR__ . '/../dictionaries/PDFShading.php';

require_once __DIR__ . '/../content/PDFContent.php';
require_once __DIR__ . '/../content/PDFText.php';
require_once __DIR__ . '/../content/PDFCanvas.php';

/**
 * A PDF document.
 *
 * @author chervbyn
 * @package pHPDF/base
 */
class PDF {

	const VERSION = '1.6';

	protected $objects = array();

	protected $catalog;
	protected $pages;
	protected $info;

	function __construct() {
		$this->catalog = new PDFCatalog();
		$this->pages = new PDFPages($this);
		$this->catalog->setPages($this->pages);
		$this->addObject($this->catalog);
		$this->addObject($this->pages);

		$this->info = new PDFInfo();
		$this->addObject($this->info);
	}

	function addObject(PDFDictionary $object) {
		if (!in_array($object, $this->objects)) {
			$this->objects[] = $object;
		} else {
			// TODO throw an IAExc. ? - would be better for testing
			syslog(4, 'PDF::addObject(): object \'' . $object->getName() . '\' not added - duplicate.');
		}
	}

	/**
	 *
	 * @param PDFPage $page a page dictionary or null if one shall be created
	 * @return PDFPage the added page dictionary
	 */
	function addPage(PDFPage $page = null) {
		if (!$page) {
			$page = new PDFPage($this);
		}
		$this->pages->addKid($page);
		$this->addObject($page);
		return $page;
	}

	/**
	 *
	 * @return PDFCatalog the catalog dictionary
	 */
	function getCatalog() {
		return $this->catalog;
	}

	/**
	 *
	 * @return PDFPages the root pages dictionary
	 */
	function getPages() {
		return $this->pages;
	}

	/**
	 *
	 * @return PDFInfo the info dictionary
	 */
	function getInfo() {
		return $this->info;
	}

	/**
	 *
	 * @return string the whole pdf document as string
	 */
	function getPDF() {
		$xref = array();
		$pdf = '%PDF-' . PDF::VERSION . "\n";
		foreach ($this->objects as $object) {
			$xref[$object->getId()] = strlen($pdf);
			$pdf .= $object->getName() . " obj\n" . $object . "\nendobj\n";
		}
		$startXref = strlen($pdf);
		$pdf .= $this->getXref($xref);
		$pdf .= $this->getTrailer();
		$pdf .= "startxref\n$startXref\n";
		$pdf .= '%%EOF';

		return $pdf;
	}

	protected function getXref($xrefObjects) {
		$xref = "xref\n";
		ksort($xrefObjects);
		$lastObjName = 0;
		$sectionStart = 0;
		$section = array("0000000000 65535 f\r\n");
		foreach ($xrefObjects as $objName => $offset) {
			if ($objName != ($lastObjName + 1)) {
				// put section header and section
				$xref .= "$sectionStart " . count($section) . "\n";
				$xref .= implode('', $section);
				$section = array();
				$sectionStart = $objName;
			}
			$section[] = sprintf("%1$010d 00000 n", $offset) . "\r\n";
			$lastObjName = $objName;
		}

		if (count($section) > 0) {
			// put last section header and section
			$xref .= "$sectionStart " . count($section) . "\n";
			$xref .= implode('', $section);
		}
		return $xref;
	}

	protected function getTrailer() {
		$trailer = new PDFDictionary();
		$trailer->put('Size', count($this->objects) + 1);
		$trailer->put('Root', $this->catalog->getLink());
		$trailer->put('Info', $this->info->getLink());
		return "trailer\n$trailer\n";
	}
}