<?php

require_once __DIR__ . '/../base/PDFStream.php';

/**
* 
* Enter description here ...
* @author chervbyn
* @package pHPDF/content
*/
class PDFContent extends PDFStream {
	
	protected $content = array();
	
	function __construct($content = null) {
		if ($content)
			$this->append($content);
	}
}