<?php

require_once __DIR__ . '/PDFContent.php';

/**
 * A Canvas object is used for drawing on a page.
 *
 * @author Steffen Rocktaeschel
 */
class PDFCanvas extends PDFContent {
  protected $source = '';
  protected $strokeColor = '';
  protected $fillColor = '';
  
  protected $currentX = 0;
  protected $currentY = 0;

  function fill() {
    if ($this->fillColor)
      $this->append(' ' . $this->fillColor);

    $this->append(' f');
  }

  function stroke() {
    if ($this->strokeColor)
      $this->append(' ' . $this->strokeColor);

    $this->append(' s');
  }

  /**
   * 
   * @param unknown $x
   * @param unknown $y
   */
  function moveTo($x, $y) {
    $this->append(" $x $y m");
    $this->currentX = $x;
    $this->currentY = $y;
  }

  function lineTo($x, $y) {
    $this->line($x, $y);
    // TODO im not sure, if its a good idea to close the current path
    // would be better to impl. a line fct. and a polyline fct.; latter should end the path
    // at its last coord.
    $this->close();
  	$this->moveTo($x, $y);
  }
  
  /**
   * Appends a straight line segment from the current point to the
   * point (x, y). The new current point is moved to (x, y).
   *
   * @param float $x
   * @param float $y
   */
  function line($x, $y) {
    $this->append(" $x $y l");
  }
  
/*  
  function polyline() {
    if (func_num_args() > 2 && func_num_args() % 2 == 0) {
      $args = func_get_args();
      $x = $args[0];
      $y = $args[1];
      $this->moveTo($x, $y);
      for ($i = 2; $i < count($args); $i += 2) {
        $x = $args[$i];
        $y = $args[$i + 1];
        $this->line($x, $y);
      }
      $this->close();
      $this->moveTo($x, $y);
    }
  }
*/
  
  /**
   * Close the current subpath by appending a straight line segment
   * from the current point to the starting point of the subpath.
   */
  function close() {
    $this->append(" h");
  }

  function poly() {
    if (func_num_args() > 2 && func_num_args() % 2 == 0) {
      $args = func_get_args();
      $this->moveTo($args[0], $args[1]);
      for ($i = 2; $i < count($args); $i += 2) {
        $x = $args[$i];
        $y = $args[$i + 1];
        $this->line($x, $y);
      }
      $this->close();
    }
  }

  /**
   * Append a rectangle to the current path as a complete
   * subpath, with lower-left corner (x, y) and dimensions width
   * and height in user space.
   * 
   * @param float $x
   * @param float $y
   * @param float $width
   * @param float $height
   */
  function rect($x, $y, $width, $height) {
    $this->append(" $x $y $width $height re");
  }

  function setSource($source) {
    $this->source = $source;
  }

  function setStrokeColor($r, $g, $b) {
    $this->strokeColor = "$r $g $b RG";
  }

  function setFillColor($r, $g, $b) {
    $this->fillColor = "$r $g $b rg";
  }

  function getStream() {
    $stream = parent::getStream();
    return $stream;
  }

}