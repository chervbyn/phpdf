<?php

require_once __DIR__ . '/PDFContent.php';

/**
* A stream object offering methods for creating text cells.
*
* @author chervbyn
* @package pHPDF/content
*/
class PDFText extends PDFContent {

	protected $font;
	protected $fontSize;
	protected $x;
	protected $y;
	protected $text = '()';

	function __construct($x = null, $y = null, $width = 0, $height = 0) {
		$this->x = $x;
		$this->y = $y;
		$this->width = $width;
		$this->height = $height;
	}

	/**
	 * Set the text of this cell.
	 *
	 * $text is converted into a string
	 *
	 * @param string $text
	 */
	function setText($text) {
		$this->text = "($text)";
	}

	function setHex($hex) {
		$this->text = "<$hex>";
	}

	function setXY($x, $y) {
		$this->x = $x;
		$this->y = $y;
	}

	function setFont($font) {
		$this->font = $font;
	}

	function setFontSize($fontSize) {
		$this->fontSize = $fontSize;
	}

	function getStream() {
		$parentStream = parent::getStream();
		$stream = array(' BT' /*. ' 1 0 0 -1 0 100 Tm' */);
		if ($this->font)
			$stream[] = "/{$this->font}";

		if ($this->fontSize)
			$stream[] = "{$this->fontSize} Tf";

		if ($this->x && $this->y)
			$stream[] = "{$this->x} {$this->y} Td";

		$stream[] = "{$this->text} Tj ET";

		if ($parentStream)
			$stream[] = $parentStream;

		return implode(' ', $stream);
	}
}

