<?php

require_once __DIR__ . '/PDFXObject.php';

/**
 * An extended PDFXObject for use as an image descriptor.
 * 
 * see 8.9.5 Image Dictionaries (S. 206)
 *
 * @author chervbyn
 * @package pHPDF/content
 */
class PDFImage extends PDFXObject {
	
	function __construct() {
		parent::__construct();
		$this->put('Subtype', '/Image');
	}
	
	/**
	 * Set the height of the image, in samples. (Required)
	 * 
	 * Set the width of the image, in samples. (Required)
	 * 
	 * @param double $width
	 */
	function setWidth($width) {
		$this->put('Width', $width);
	}

	/**
	 * Set the width of the image, in samples. (Required)
	 * 
	 * Set the width of the image, in samples. (Required)
	 * 
	 * @param double $height
	 */
	function setHeight($height) {
		$this->put('Height', $height);
	}
	
	/**
	 * Set the image's color space.
	 * 
	 * (Required for images, except those that use the JPXDecode filter; not allowed forbidden for
	 * image masks) The colour space in which image samples shall be specified; it can be any type
	 * of colour space except Pattern. If the image uses the JPXDecode filter,
	 * this entry may be present:
	 * 
	 * 1) If ColorSpace is present, any colour space specifications in the JPEG2000 data shall be ignored.
	 * 2) If ColorSpace is absent, the colour space specifications in the JPEG2000 data shall be used.
	 * The Decode array shall also be ignored unless ImageMask is true.

	 * Enter description here ...
	 * @param unknown_type $colorSpace
	 */
	function setColorSpace($colorSpace) {
		$this->put('ColorSpace', "/$colorSpace");
	}
	
	/**
	 * Set the number of bits used to represent each colour component.
	 * 
	 * Required except for image masks and images that use the JPXDecode filter) The number of
	 * bits used to represent each colour component. Only a single value shall be specified; the
	 * number of bits shall be the same for all colour components. The value shall be
	 * 1, 2, 4, 8, or (in PDF 1.5) 16. If ImageMask is true, this entry is optional, but if
	 * specified, its value shall be 1. If the image stream uses a filter, the value of
	 * BitsPerComponent shall be consistent with the size of the data samples that the filter
	 * delivers. In particular, a CCITTFaxDecode or JBIG2Decode filter shall always deliver 1-bit
	 * samples, a RunLengthDecode or DCTDecode filter shall always deliver 8-bit samples, and an
	 * LZWDecode or FlateDecode filter shall deliver samples of a specified size if a predictor
	 * function is used. If the image stream uses the JPXDecode filter, this entry is optional
	 * and shall be ignored if present. The bit depth is determined by the conforming reader in
	 * the process of decoding the JPEG2000 image.
	 * 
	 * @param int $bpc
	 */
	function setBitsPerComponent($bpc) {
		$this->put('BitsPerComponent', $bpc);
	}
}