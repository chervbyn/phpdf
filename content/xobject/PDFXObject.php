<?php

require_once __DIR__ . '/../../base/PDFStream.php';

class PDFXObject extends PDFStream {
	
	function __construct() {
		$this->put('Type', '/XObject');
	}
}