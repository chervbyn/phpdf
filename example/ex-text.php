<?php
require_once '../pHPDF.php';
require_once '../streams/PDFText.php';
require_once '../streams/PDFTextLine.php';

$pdf = new pHPDF();
$page = $pdf->addPage();

$page->setFont('FONTArial');
$page->setFontSize(40);

$t = new PDFTextBox(100, 610);
$t->setFontSize(40);
$t->addText("Hello,\nI'm a quick brown fox.");
$page->appendContent($t);


$pdfString = $pdf->getPDF();
file_put_contents(__DIR__ . '/render/ex-text.pdf', $pdfString);
