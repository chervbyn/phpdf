<?php
require_once __DIR__ . '/../pHPDF.php';

$pdf = new pHPDF();
// create a page
$page = $pdf->addPage();

// create a canvas
$c = $page->addCanvas();

// rect
$c->setStrokeColor(0.5, 0.5, 0.5);
$c->rect(10, 831 - 100, 100, 100);
$c->stroke();

$c->rect(120, 831 - 100, 100, 100);
$c->setFillColor(0, 0.75, 0);
$c->fill();

// moveto, lineto
$c->moveTo(10, 700);
$c->lineTo(530, 700);
$c->lineTo(530, 600);
$c->setStrokeColor(1, 0, 0);
$c->stroke();
$c->moveTo(530, 600);
$c->lineTo(10, 600);
$c->lineTo(10, 700);
$c->setStrokeColor(0, 0, 1);
$c->stroke();

// $this->poly(x1, y1, x2, y2, ...)
$c->poly(10, 500, 530, 500, 530, 400, 400, 350);
$c->setFillColor(0.7, 0.5, 0.3);
$c->fill();


$pdfString = $pdf->getPDF();
echo $pdfString;
file_put_contents(__DIR__ . '/render/ex-canvas.pdf', $pdfString);
