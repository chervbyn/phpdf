<?php

ini_set('display_error', 1);
require_once '../util/ImageParser.php';
require_once '../base/PDF.php';

$pdf = new PDF();
$page = $pdf->addPage();

// JPG
$page->getResources()->put('ProcSet', '[ /PDF /Text /ImageB /ImageC /ImageI ]');
$imgWidth = 500;
$page->addImage(__DIR__ . '/images/phlogo.jpg', 596.59 / 4, 50, 596.59 / 2);
$text = $page->addText();

// echo $pdf->getPDF();
file_put_contents(__DIR__ . '/render/ex-jpeg.pdf', $pdf->getPDF());

