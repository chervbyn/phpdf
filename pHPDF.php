<?php

require_once __DIR__ . '/pHPDFPage.php';
require_once __DIR__ . '/base/PDF.php';
/**
 * @author chervbyn
 * @codeCoverageIgnore
 */
class pHPDF {

	protected $pdf;
	protected $currentPage;
	protected $currentContentStream;

	function __construct() {
		$this->pdf = new PDF();
		global $phpdf;
		$phpdf = $this->pdf;
	}

	/**
	 * Adds a new page.
	 *
	 * @return pHPDFPage a page object
	 */
	function addPage() {
		$page = new pHPDFPage($this->pdf);
		$this->pdf->addPage($page);
		return $page;
	}

	function getPDF() {
		return $this->pdf->getPDF();
	}
}