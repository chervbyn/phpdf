<?php

require_once __DIR__ . '/base/PDF.php';
require_once __DIR__ . '/base/PDFStream.php';

/**
* A pHPDF text cell.
*
* Represents a text cell.
*
* @author chervbyn
* @package pHPDF
*/
class pHPDFText extends PDFText {


}