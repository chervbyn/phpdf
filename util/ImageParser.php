<?php

require_once __DIR__ . '/../content/xobject/PDFImage.php';

class ImageParser {
	
	const TYPE_GIF = 1;
	const TYPE_JPG = 2;
	const TYPE_PNG = 3;
	
	/**
	 * 
	 * @param string $fileName
	 * @return PDFXOject the image dict
	 */
	static function parseFile($fileName) {
		// Extract info from a JPEG file
		$imageInfo = getimagesize($fileName);
		
		if(!$imageInfo) {
			self::error('Missing or incorrect image file: '.$fileName);
		}
		
		$imgType = $imageInfo[2];
		$xObject = null;
		switch($imgType) {
		case self::TYPE_JPG:
			$xObject = self::parseJPGFile($fileName, $imageInfo);
			break;
			
		case self::TYPE_PNG:
			$x = self::_parsepng($fileName);
			$xObject = new PDFImage();
			$xObject->setBitsPerComponent($x['bpc']);
			$xObject->setColorSpace($x['cs']);
			$xObject->setFilter($x['f'], false);
			$xObject->setWidth($x['w']);
			$xObject->setHeight($x['h']);
			$xObject->put('DecodeParms', '<< ' . $x['dp'] . ' >>');
			$xObject->append($x['data']);
			break;
			
		case self::TYPE_GIF:
			$xObject = self::_parsegif($fileName);
			break;
		
		default:
			break;
		}
		
		return $xObject;
	}
	
	static function error($msg) {
		syslog(4, "ERROR JPGParser: $msg");
	}
	
	static function parseJPGFile($fileName, $imageInfo) {
		$xObject = new PDFImage();
		
		if(!isset($imageInfo['channels']) || $imageInfo['channels'] == 3)
			$xObject->setColorSpace('DeviceRGB');
		else if($imageInfo['channels'] == 4)
			$xObject->setColorSpace('DeviceCMYK');
		else
			$xObject->setColorSpace('DeviceGray');
		
		$bpc = isset($imageInfo['bits']) ? $imageInfo['bits'] : 8;
		$xObject->setBitsPerComponent($bpc);
		$xObject->setWidth($imageInfo[0]);
		$xObject->setHeight($imageInfo[1]);
		$xObject->setFilter(PDFStream::FILTER_DCTDECODE, false);
		$xObject->append(file_get_contents($fileName));
		return $xObject;
	}
	
	// PNG
	
	static function _parsepng($file)
	{
		// Extract info from a PNG file
		$f = fopen($file,'rb');
		if(!$f)
		self::error('Can\'t open image file: '.$file);
		$info = self::_parsepngstream($f,$file);
		fclose($f);
		return $info;
	}
	
	static function _parsepngstream($f, $file)
	{
		// Check signature
		if(self::_readstream($f,8)!=chr(137).'PNG'.chr(13).chr(10).chr(26).chr(10))
			self::error('Not a PNG file: '.$file);
	
		// Read header chunk
		self::_readstream($f,4);
		if(self::_readstream($f,4)!='IHDR')
			self::error('Incorrect PNG file: '.$file);
		$w = self::_readint($f);
		$h = self::_readint($f);
		$bpc = ord(self::_readstream($f,1));
		if($bpc>8)
			self::error('16-bit depth not supported: '.$file);
		$ct = ord(self::_readstream($f,1));
		if($ct==0 || $ct==4)
			$colspace = 'DeviceGray';
		else if($ct==2 || $ct==6)
			$colspace = 'DeviceRGB';
		else if($ct==3)
			$colspace = 'Indexed';
		else
			self::error('Unknown color type: '.$file);
		if(ord(self::_readstream($f,1))!=0)
			self::error('Unknown compression method: '.$file);
		if(ord(self::_readstream($f,1))!=0)
			self::error('Unknown filter method: '.$file);
		if(ord(self::_readstream($f,1))!=0)
			self::error('Interlacing not supported: '.$file);
		self::_readstream($f,4);
		$dp = '/Predictor 15 /Colors '.($colspace=='DeviceRGB' ? 3 : 1).' /BitsPerComponent '.$bpc.' /Columns '.$w;
	
		// Scan chunks looking for palette, transparency and image data
		$pal = '';
		$trns = '';
		$data = '';
		do
		{
			$n = self::_readint($f);
			$type = self::_readstream($f,4);
			if($type=='PLTE')
			{
				// Read palette
				$pal = self::_readstream($f,$n);
				self::_readstream($f,4);
			}
			elseif($type=='tRNS')
			{
				// Read transparency info
				$t = self::_readstream($f,$n);
				if($ct==0)
					$trns = array(ord(substr($t,1,1)));
				else if($ct==2)
					$trns = array(ord(substr($t,1,1)), ord(substr($t,3,1)), ord(substr($t,5,1)));
				else
				{
					$pos = strpos($t,chr(0));
					if($pos!==false)
					$trns = array($pos);
				}
				self::_readstream($f,4);
			}
			elseif($type=='IDAT')
			{
				// Read image data block
				$data .= self::_readstream($f,$n);
				self::_readstream($f,4);
			}
			else if($type=='IEND')
				break;
			else
				self::_readstream($f,$n+4);
		}
		while($n);
	
		if($colspace=='Indexed' && empty($pal))
			self::error('Missing palette in '.$file);
		$info = array('w'=>$w, 'h'=>$h, 'cs'=>$colspace, 'bpc'=>$bpc, 'f'=>'FlateDecode', 'dp'=>$dp, 'pal'=>$pal, 'trns'=>$trns);
		if($ct>=4)
		{
			// Extract alpha channel
			if(!function_exists('gzuncompress'))
			self::error('Zlib not available, can\'t handle alpha channel: '.$file);
			$data = gzuncompress($data);
			$color = '';
			$alpha = '';
			if($ct==4)
			{
				// Gray image
				$len = 2*$w;
				for($i=0;$i<$h;$i++)
				{
					$pos = (1+$len)*$i;
					$color .= $data[$pos];
					$alpha .= $data[$pos];
					$line = substr($data,$pos+1,$len);
					$color .= preg_replace('/(.)./s','$1',$line);
					$alpha .= preg_replace('/.(.)/s','$1',$line);
				}
			}
			else
			{
				// RGB image
				$len = 4*$w;
				for($i=0;$i<$h;$i++)
				{
					$pos = (1+$len)*$i;
					$color .= $data[$pos];
					$alpha .= $data[$pos];
					$line = substr($data,$pos+1,$len);
					$color .= preg_replace('/(.{3})./s','$1',$line);
					$alpha .= preg_replace('/.{3}(.)/s','$1',$line);
				}
			}
			unset($data);
			$data = gzcompress($color);
			$info['smask'] = gzcompress($alpha);
		}
		$info['data'] = $data;
		return $info;
	}
	
	static function _readstream($f, $n)
	{
		// Read n bytes from stream
		$res = '';
		while($n>0 && !feof($f))
		{
			$s = fread($f,$n);
			if($s===false)
			self::error('Error while reading stream');
			$n -= strlen($s);
			$res .= $s;
		}
		if($n>0)
		self::error('Unexpected end of stream');
		return $res;
	}
	
	static function _readint($f)
	{
		// Read a 4-byte integer from stream
		$a = unpack('Ni',self::_readstream($f,4));
		return $a['i'];
	}
	
	// GIF
	
	function _parsegif($file)
	{
		// Extract info from a GIF file (via PNG conversion)
		if(!function_exists('imagepng'))
		self::error('GD extension is required for GIF support');
		if(!function_exists('imagecreatefromgif'))
		self::error('GD has no GIF read support');
		$im = imagecreatefromgif($file);
		if(!$im)
		self::error('Missing or incorrect image file: '.$file);
		imageinterlace($im,0);
		$f = @fopen('php://temp','rb+');
		if($f)
		{
			// Perform conversion in memory
			ob_start();
			imagepng($im);
			$data = ob_get_clean();
			imagedestroy($im);
			fwrite($f,$data);
			rewind($f);
			$info = self::_parsepngstream($f,$file);
			fclose($f);
		}
		else
		{
			// Use temporary file
			$tmp = tempnam('.','gif');
			if(!$tmp)
			self::error('Unable to create a temporary file');
			if(!imagepng($im,$tmp))
			self::error('Error while saving to temporary file');
			imagedestroy($im);
			$info = self::_parsepng($tmp);
			unlink($tmp);
		}
		
		$xObject = new PDFImage();
		$xObject->setBitsPerComponent($info['bpc']);
		$xObject->setFilter($info['f'], false);
		$xObject->setWidth($info['w']);
		$xObject->setHeight($info['h']);
		$xObject->put('DecodeParms', '<< ' . $info['dp'] . ' >>');
		$xObject->append($info['data']);
		global $phpdf;
		$cs = new PDFStream();
		$cs->append(file_get_contents('/tmp/gifcs'));
 		$phpdf->addObject($cs);
 		$xObject->put('ColorSpace', "[ /Indexed /DeviceRGB 125 {$cs->getLink()} ]");
//		$xObject->put('')
		
		return $xObject;
	}
/*
 function _putimage(&$info)
{
	$this->_newobj();
	$info['n'] = $this->n;
	$this->_out('<</Type /XObject');
	$this->_out('/Subtype /Image');
	$this->_out('/Width '.$info['w']);
	$this->_out('/Height '.$info['h']);
	if($info['cs']=='Indexed')
		$this->_out('/ColorSpace [/Indexed /DeviceRGB '.(strlen($info['pal'])/3-1).' '.($this->n+1).' 0 R]');
	else
	{
		$this->_out('/ColorSpace /'.$info['cs']);
		if($info['cs']=='DeviceCMYK')
			$this->_out('/Decode [1 0 1 0 1 0 1 0]');
	}
	$this->_out('/BitsPerComponent '.$info['bpc']);
	if(isset($info['f']))
		$this->_out('/Filter /'.$info['f']);
	if(isset($info['dp']))
		$this->_out('/DecodeParms <<'.$info['dp'].'>>');
	if(isset($info['trns']) && is_array($info['trns']))
	{
		$trns = '';
		for($i=0;$i<count($info['trns']);$i++)
			$trns .= $info['trns'][$i].' '.$info['trns'][$i].' ';
		$this->_out('/Mask ['.$trns.']');
	}
	if(isset($info['smask']))
		$this->_out('/SMask '.($this->n+1).' 0 R');
	$this->_out('/Length '.strlen($info['data']).'>>');
	$this->_putstream($info['data']);
	$this->_out('endobj');
	// Soft mask
	if(isset($info['smask']))
	{
		$dp = '/Predictor 15 /Colors 1 /BitsPerComponent 8 /Columns '.$info['w'];
		$smask = array('w'=>$info['w'], 'h'=>$info['h'], 'cs'=>'DeviceGray', 'bpc'=>8, 'f'=>$info['f'], 'dp'=>$dp, 'data'=>$info['smask']);
		$this->_putimage($smask);
	}
	// Palette
	if($info['cs']=='Indexed')
	{
		$filter = ($this->compress) ? '/Filter /FlateDecode ' : '';
		$pal = ($this->compress) ? gzcompress($info['pal']) : $info['pal'];
		$this->_newobj();
		$this->_out('<<'.$filter.'/Length '.strlen($pal).'>>');
		$this->_putstream($pal);
		$this->_out('endobj');
	}
}
*/

}