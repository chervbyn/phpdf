<?php

require_once __DIR__ . '/../base/PDFDictionary.php';

class PDFDictionaryParser {
	
	/**
	 * Tests two dictonaries for equality.
	 * 
	 * @param PDFDictionary|string $dict1
	 * @param PDFDictionary|string $dict2
	 */
	static function equals($dict1, $dict2) {
		if (!$dict1 instanceof PDFDictionary)
			$dict1 = self::parse($string);
		if (!$dict2 instanceof PDFDictionary)
			$dict2 = self::parse($string);
		
		if ($dict1->size() == $dict2->size()) {
			$keys1 = $dict1->keys();
			foreach ($keys1 as $key) {
				if ($dict1->get($key) != $dict2->get($key)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param string $string
	 * @return PDFDictionary
	 */
	static function parse($string) {
		$dict = new PDFDictionary();
		$pStrings = "\([^\)]*\)";
		$pArrays = "\[[^\]]*\]";
		$pDict = "<<[^>]*>>";
		$pattern = "/\/([A-Za-z0-9_]+)\s+(\/[A-Za-z0-9_]+|[0-9]+ 0 R|[0-9]+|$pDict|$pArrays|$pStrings)/s";
		preg_match_all($pattern, $string, $match);
		
		$keys = $match[1];
		$values = $match[2];
		$entries = array();
		foreach ($keys as $index => $key) {
			$value = $values[$index];
			$dict->put($key, $value);	
		}
		return $dict;
	}
}