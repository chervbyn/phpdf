Hello,

My intention is to to build a modular PDF and SVG lib which provide the same interface
for drawing and writing operations, so that they could be used exchangably. This is the PDF part of it.
I started devleoping in Nov. 2011 but had no time/motivation since then to proceed. Now, one year has passed,
it got reanimated and uploaded to BB. I will commit changes from time to time.

If you have sth. to say, to ask, to contribute in any way: don't hesitate to contact me.
Questions, criticism, hints, patches - everything is very welcome.

It's in a very early stage. At this point one can:

* not use this for production yet

* create a document

* add pages

* add a canvas to a page, and perform some basic drawing operations on it

* add an image to a page

* add text to a page (really basic)
 
Next steps could be:

* abstract interface definition (drawing, text)

* write more unit tests

* reorganize code

* improve text boxes, text lines or take another approach
   goal: process text with markup for style, font, font size ... like: Hello {#i: World} should print "World" in italics;
   so there should be defined some kind of markup lang too
   
* speacial chars support for text

* include font files

* change page/object origin from bottom left corner (PDF style) to top left corner

* improve image support
 
* more path manipulation for canvas

