<?php

require_once __DIR__ . '/pHPDFCanvas.php';
require_once __DIR__ . '/pHPDFText.php';
require_once __DIR__ . '/base/PDF.php';
require_once __DIR__ . '/base/PDFStream.php';
require_once __DIR__ . '/content/PDFContent.php';
require_once __DIR__ . '/content/xobject/PDFXObject.php';
require_once __DIR__ . '/content/xobject/PDFImage.php';
require_once __DIR__ . '/dictionaries/PDFPage.php';
require_once __DIR__ . '/dictionaries/PDFFont.php';
require_once __DIR__ . '/util/ImageParser.php';

/**
 * A pHPDF page.
 *
 * Represents a page and it's main content stream.
 *
 * @author chervbyn
 * @package pHPDF
 * @codeCoverageIgnore
 */
class pHPDFPage extends PDFPage {

	protected $pdf;
	protected $contentStream;
	protected $font = 'Arial';
	protected $fontSize = 12;
	protected $imgCounter = 0;
	protected $width = 596.59;
	protected $height = 842.55;

	function __construct(PDF $pdf) {
		parent::__construct($pdf);
		$contentStream = new PDFContent();
		$pdf->addObject($contentStream);
		$this->addContent($contentStream);
		$this->setMediaBox(0, 0, $this->width, $this->height);
		$this->pdf = $pdf;
		$this->contentStream = $contentStream;

		// test
		$f = new PDFFont('Arial', PDFFont::TYPE_3);
		$pdf->addObject($f);
		$this->getResources()->addFont('FONTArial', $f);
		$f = new PDFFont('Courier', PDFFont::TYPE_3);
		$pdf->addObject($f);
		$this->getResources()->addFont('FONTCourier', $f);


/*
		$f2 = new PDFStream();
		$s = file_get_contents(__DIR__ . '/test/FreeMono.ttf');
		$l = strlen($s);
		$f2->put('Length1', $l);
		$f2->append($s);
		$pdf->addObject($f2);



// 		$fd = new PDFDictionary();
// 		$fd->put('Type', '/FontDescriptor');
// 		$fd->put('FontName', '/Arial');
//   		$fd->put('Flags', '34'); // '262178');
// 		$fd->put('FontBBox', '[ −177 −269 1123 866 ]');
// 		$fd->put('MissingWidth', '255');
// 		$fd->put('StemV', '105');
// 		$fd->put('StemH', '45');
// 		$fd->put('CapHeight', '660');
// 		$fd->put('XHeight', '394');
// 		$fd->put('Ascent', '720');
// 		$fd->put('Descent', '−270');
// 		$fd->put('Leading', '83');
//  		$fd->put('MaxWidth', '1212');
//  		$fd->put('AvgWidth', '478');
//  		$fd->put('ItalicAngle', '0');
		$fd = new PDFDictionary();
		$fd->put('Type', '/FontDescriptor');
   		$fd->put('Flags', '1'); // '262178');
		$fd->put('FontName', '/FreeMono');
		$fd->put('FontFile2', $f2->getLink());

		$pdf->addObject($fd);

		$f = new PDFFont('FreeMono');
		$f->put('FontDescriptor', $fd->getLink());
		$this->getResources()->addFont('F1', $f);
		$pdf->addObject($f);
*/
	}

	function addText($text, $x, $y, $width, $height) {
		$textCell = new pHPDFText($this->pdf);
		$textCell->setText($text);
		$textCell->setXY($x, $y);
		$textCell->setFont($this->font);
		$textCell->setFontSize($this->fontSize);
		$this->contentStream->append($textCell);
		return $textCell;
	}

	function setFont($font) {
		$this->font = $font;
	}

	function setFontSize($fontSize) {
		$this->fontSize = $fontSize;
	}

	function addImage($file, $x, $y, $width = null, $height = null) {
		$xo = ImageParser::parseFile($file);
		if ($xo) {
			if ($width === null && $height === null) {
				$width = $xo->get('Width');
				$height = $xo->get('Height');
			} else if ($width === null) {
				// fixed width
				$width = ($height / $xo->get('Height')) * $xo->get('Width');
			} else if ($height === null) {
				// fixed height
				$height = ($width / $xo->get('Width')) * $xo->get('Height');
			}

			$yMirror = $this->height - $y - $height;

			$this->getResources()->addXObject('IMG' . $this->imgCounter, $xo);
			$this->pdf->addObject($xo);
			$this->contentStream->append(" 2 J 0.57 w q $width 0 0 $height $x {$yMirror} cm /IMG{$this->imgCounter} Do Q ");
			$this->imgCounter++;
		}
	}

	function getWidth() {
		return $this->width;
	}

	function getHeight() {
		return $this->height;
	}

	function appendContent(PDFContent $content) {
		$this->contentStream->append($content);
	}
}